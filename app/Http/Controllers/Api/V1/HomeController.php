<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Services\ProductServices;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request,ProductServices $productServices){
        $products = $productServices->getProducts($request);
        return ProductResource::collection($products);
    }
}
