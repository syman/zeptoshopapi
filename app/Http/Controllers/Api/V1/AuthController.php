<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Http\Resources\UserResource;
use App\Models\User;

use App\Services\AuthServices;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function registration(RegistrationRequest $request, AuthServices $authServices){

        $user = $authServices->handleRegistration($request);


        return new UserResource($user);
    }

    public function login(LoginRequest $request){


        $user = User::where('email', $request->email)->first();
        if (! $user || ! Hash::check($request->password, $user->password)) {
            return response([
                'message' => 'The provided credentials are incorrect.'
            ],401);
        }
        else {

            return new UserResource($user);
        }
    }
//
    public function logout() {
        auth()->user()->tokens()->delete();
        return response(null,204);
    }

}
