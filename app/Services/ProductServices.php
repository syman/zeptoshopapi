<?php


namespace App\Services;


use App\Models\Product;

class ProductServices
{
        public function getProducts($request){
            $products = Product::orderBy('id','desc');
            if($request->search){
                $products = $products->where('title','like','%'.$request->search.'%');
            }
            $products = $products->paginate(10);
            return $products;
        }

        public function storeProduct($request){
            $data = $request->all();
            $request->file('image')->store('uploads/products','local');
            $image = $request->file('image')->hashName();
            $data['image'] = $image;
            return Product::create($data);
        }

        public function updateProduct($request,$product){
            $data = $request->all();
            if ($request->file('image')){
                $request->file('image')->store('uploads/products','local');
                $image = $request->file('image')->hashName();
                $data['image'] = $image;
            }
            $product->update($data);
//            $pro = Product::find($pro_id);
            return $product;
        }
}
