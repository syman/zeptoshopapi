<?php


namespace App\Services;


use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthServices
{

        public function handleRegistration($request){
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'role' => 'user',
                'password' => Hash::make($request->password)
            ]);

            return $user;
        }
}
