<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static paginate(int $int)
 * @method static create(array $data)
 * @method static find(int $id)
 */
class Product extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    protected $fillable = ['title','price','image'];
}
