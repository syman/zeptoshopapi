<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('products', \App\Http\Controllers\Api\V1\ProductController::class,['except' => ['create', 'edit','update']]);
Route::post('products/{product}',[\App\Http\Controllers\Api\V1\ProductController::class,'update'])->name('products.update');
Route::middleware('auth:sanctum')->post('logout', [\App\Http\Controllers\Api\V1\AuthController::class,'logout']);
Route::post('registration',[\App\Http\Controllers\Api\V1\AuthController::class,'registration']);
Route::post('login',[\App\Http\Controllers\Api\V1\AuthController::class,'login']);
Route::get('/',[\App\Http\Controllers\Api\V1\HomeController::class,'index']);
